#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <string>
#include <array>
#include <math.h>
#include <time.h>


using namespace std;

// to compile g++ -O3  main4.cpp -o sixloci


int read_parameter(const char* input_file, double& s1, double& s2, double& s3, double& s4, double& s5, double& s6, double& e12, double& e34, double& e56, double& e13, double& e14, double& e15, double& e16, double& e23, double& e24, double& e25, double& e26, double& e35, double& e36, double& e45, double& e46, double& r12, double& r23, double& r34, double& r45, double& r56, int& n_gen, int& ini_hap, int& mig_hap, double& mig_rate, int& rounding, int& cutoff, int& fix_freq){

    int test;
    FILE * param_file;

    // read the parameter file; check that the different entries of the parameter file matches the expected type and store this value in the proper variable.

    param_file= fopen(input_file,"r");
    if (param_file == NULL) {
            cout << "wrong param file" << endl;
        return -1;
    }

    test=fscanf(param_file,"%lf ",&s1);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&s2);
    if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&s3);
    if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&s4);
    if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&s5);
    if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&s6);
   	if (test!=1){
    	cout << "error param file"<<endl;
            return -1;
    }
    test=fscanf(param_file,"%lf ",&e12);
    if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&e34);
  	if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&e56);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e13);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e14);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e15);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e16);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e23);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e24);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e25);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e26);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e35);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e36);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e45);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&e46);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&r12);
   	if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&r23);
    if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&r34);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%lf ",&r45);
    if (test!=1){
    	cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&r56);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%i ",&n_gen);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%i ",&mig_hap);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%lf ",&mig_rate);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
    test=fscanf(param_file,"%i ",&ini_hap);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	 test=fscanf(param_file,"%i ",&rounding);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%i ",&cutoff);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
	test=fscanf(param_file,"%i ",&fix_freq);
    if (test!=1){
        cout << "error param file"<<endl;
        return -1;
    }
    fclose(param_file);
    return 1;
} // Updated


void gametes_prod(array<double, 64>& hap, const array<double, 64>& hap_ini, const double& r12, const double& r23, const double& r34, const double& r45, const double& r56){
    // define the gamete production by considering the allele at each locus from the paternal (i.m) and maternal (i.f) origin
	int indicem, indicef, new_indicem, new_indicef;
    for (int i=0; i<64; i++){
        hap[i]=0;
    }


  for (int i1m=0; i1m<=1; i1m++){
    for (int i2m=0; i2m<=1; i2m++){
      for (int i3m=0; i3m<=1; i3m++ ){
        for (int i4m=0; i4m<=1; i4m++){
          for (int i5m=0; i5m<=1; i5m++){
            for (int i6m=0; i6m<=1; i6m++){
							indicem =32*i1m+16*i2m+8*i3m+4*i4m+2*i5m+i6m; // define the focus haplotype
							for (int i1f=0; i1f<=1; i1f++){
								for (int i2f=0; i2f<=1; i2f++){
									for (int i3f=0; i3f<=1; i3f++ ){
										for (int i4f=0; i4f<=1; i4f++){
											for (int i5f=0; i5f<=1; i5f++){
												for (int i6f=0; i6f<=1; i6f++){
													indicef =32*i1f+16*i2f+8*i3f+4*i4f+2*i5f+i6f; // define the interacting haplotype
													if (indicem!=indicef){
													// rec 12
												    hap[indicem]=hap[indicem]-r12*(hap_ini[indicem]*hap_ini[indicef])/2;
                            hap[indicef]=hap[indicef]-r12*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"remove" << r12*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
												    new_indicem=32*i1m+16*i2f+8*i3f+4*i4f+2*i5f+i6f;
												    new_indicef=32*i1f+16*i2m+8*i3m+4*i4m+2*i5m+i6m;
												    hap[new_indicem]=hap[new_indicem]+r12*(hap_ini[indicem]*hap_ini[indicef])/2;
												    hap[new_indicef]=hap[new_indicef]+r12*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (new_indicef==2 || new_indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"add" << r12*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
													// rec 23
												    hap[indicem]=hap[indicem]-r23*(hap_ini[indicem]*hap_ini[indicef])/2;
                            hap[indicef]=hap[indicef]-r23*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"remove" << r23*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
												    new_indicem=32*i1m+16*i2m+8*i3f+4*i4f+2*i5f+i6f;
												    new_indicef=32*i1f+16*i2f+8*i3m+4*i4m+2*i5m+i6m;
												    hap[new_indicem]=hap[new_indicem]+r23*(hap_ini[indicem]*hap_ini[indicef])/2;
												    hap[new_indicef]=hap[new_indicef]+r23*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (new_indicef==2 || new_indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"add" << r23*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
													// rec 34
												    hap[indicem]=hap[indicem]-r34*(hap_ini[indicem]*hap_ini[indicef])/2;
                            hap[indicef]=hap[indicef]-r34*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"remove" << r34*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
												    new_indicem=32*i1m+16*i2m+8*i3m+4*i4f+2*i5f+i6f;
						                new_indicef=32*i1f+16*i2f+8*i3f+4*i4m+2*i5m+i6m;
												    hap[new_indicem]=hap[new_indicem]+r34*(hap_ini[indicem]*hap_ini[indicef])/2;
												    hap[new_indicef]=hap[new_indicef]+r34*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (new_indicef==2 || new_indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"add" << r34*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
													// rec 45
												    hap[indicem]=hap[indicem]-r45*(hap_ini[indicem]*hap_ini[indicef])/2;
                            hap[indicef]=hap[indicef]-r45*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"remove" << r45*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
												    new_indicem=32*i1m+16*i2m+8*i3m+4*i4m+2*i5f+i6f;
												    new_indicef=32*i1f+16*i2f+8*i3f+4*i4f+2*i5m+i6m;
												    hap[new_indicem]=hap[new_indicem]+r45*(hap_ini[indicem]*hap_ini[indicef])/2;
												    hap[new_indicef]=hap[new_indicef]+r45*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (new_indicef==2 || new_indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"add" << r45*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
													// rec 56
												    hap[indicem]=hap[indicem]-r56*(hap_ini[indicem]*hap_ini[indicef])/2;
                            hap[indicef]=hap[indicef]-r56*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"remove" << r56*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
												    new_indicem=32*i1m+16*i2m+8*i3m+4*i4m+2*i5m+i6f;
										        new_indicef=32*i1f+16*i2f+8*i3f+4*i4f+2*i5f+i6m;
												    hap[new_indicem]=hap[new_indicem]+r56*(hap_ini[indicem]*hap_ini[indicef])/2;
												    hap[new_indicef]=hap[new_indicef]+r56*(hap_ini[indicem]*hap_ini[indicef])/2;
													//if (new_indicef==2 || new_indicem==2){cout << "ini_hap" <<indicem << "inter_hap"<< indicef<<"add" << r56*(hap_ini[indicem]*hap_ini[indicef])<<"\n";}
												  }
												}
                			}
                		}
            			}
              	}
         			}
          	}
    			}
    		}
			}
		}
	}
} //updated


// run is the main function. There is three version of it. The definition has been overloaded, i.e. each version has different number of parameters.
// One version is the main function used to indeed run the simulations. The second one print in an extra the frequency at each generation, while the third one also output the fitness of each haplotype.
void run(const int& n_gen, const double& r12, const double& r23, const double& r34, const double& r45, const double& r56, const double& mig_rate, const int& mig_hap, const array<double, 64>& fitness_table, const array<double, 64>& initial_freq,  double& f1, double& f2, double& f3, double& f4, double& f5, double& f6, int& final_gen, double & change, const int& rounding_nb, const int& cutoff_nb){

	int indicem;
	double wbar;


  	array<double, 64> haplotype_freq; // define an array to store the frequency of all 16 different haplotypes.
  	array<double, 64> haplotype_sel;
  	array<double, 64> haplotype_rec;
    array<double, 64> haplotype_mig;

  	for (int i=0; i<64; i++){
  		haplotype_freq[i]=initial_freq[i];
        haplotype_rec[i]=initial_freq[i];
  	}



	  // calculate the genotype frequency based on the haplotype assuming no assortative mating.
	for (int gen=1; gen<=n_gen; gen++){


  	// apply selection to young haploid adults; this happens in two stages. First raw selection coefficient are multiplied to the frequencies of the genotypes.
  	//In a second step, we correct the selection value by the mean fitness wbar of the population
  	wbar=0;
  	for (int i=0; i<64; i++){
 //           cout << genotype_freq[i] <<endl;
    	haplotype_sel[i]=haplotype_freq[i]*fitness_table[i];
    	wbar=wbar+haplotype_sel[i];
  	}

		//        cout << "genotypes"<<endl;

		// correction by the mean fitness
  	for (int i=0; i<64; i++){
    	haplotype_sel[i]=haplotype_freq[i]*(fitness_table[i]-wbar);
    // cout << i <<" "<<genotype_freq[i] <<endl;
  	}

  	// apply recombination
  	gametes_prod(haplotype_rec, haplotype_freq,r12,r23,r34,r45,r56);


    //apply migration and normalze selection and recombination


    for (int i=0; i<64; i++){
      haplotype_mig[i]=-haplotype_freq[i]*mig_rate;
      haplotype_rec[i]=round(haplotype_rec[i]*pow(10,rounding_nb))/pow(10,rounding_nb);
      haplotype_sel[i]=round(haplotype_sel[i]*pow(10,rounding_nb))/pow(10,rounding_nb);
    }
    haplotype_mig[mig_hap]=haplotype_mig[mig_hap]+mig_rate;


    double total_freq=0;
    change=0;
  	for (int i=0; i<64; i++){
		change=change+fabs(haplotype_sel[i]+haplotype_rec[i]+haplotype_mig[i]);
		haplotype_freq[i]= haplotype_freq[i]+haplotype_sel[i]+haplotype_rec[i]+haplotype_mig[i];
		total_freq=total_freq+haplotype_freq[i];
        //cout << i <<" "<<abs(haplotype_sel[i]+haplotype_rec[i]+haplotype_mig[i]) <<endl;
  	}

    for (int i=0; i<64; i++){
        haplotype_freq[i]=haplotype_freq[i]/total_freq;
        //if (abs(haplotype_sel[i]+haplotype_rec[i]+haplotype_mig[i])>pow(10,-10)){
        //cout << "gen"<< gen<< "hap"<<i<<"  fin "<<haplotype_freq[i]<< " sel " << haplotype_sel[i] << " rec " <<  haplotype_rec[i] << " mig " << haplotype_mig[i] << "\n";
      //}
    }

    //cout <<change<< "\n";
    //cout <<pow(10,-cutoff_nb)<<"\n";
    //cout <<gen<< "\n";

    if (change<pow(10,-cutoff_nb) | gen==n_gen){
      for (int i1m=0; i1m<=1; i1m++){
        for (int i2m=0; i2m<=1; i2m++){
          for (int i3m=0; i3m<=1; i3m++){
            for (int i4m=0; i4m<=1; i4m++){
              for (int i5m=0; i5m<=1; i5m++){
                for (int i6m=0; i6m<=1; i6m++){
                  indicem =32*i1m+16*i2m+8*i3m+4*i4m+2*i5m+i6m;

                  cout << "gen"<< gen<< "hap"<<indicem<<"  ini "<<haplotype_freq[indicem]<< " sel " << haplotype_sel[indicem] << " rec " <<  haplotype_rec[indicem] << " mig " << haplotype_mig[indicem] << "\n";
                  f1 = f1 + i1m * haplotype_freq[indicem];
                  f2 = f2 + i2m * haplotype_freq[indicem];
                  f3 = f3 + i3m * haplotype_freq[indicem];
                  f4 = f4 + i4m * haplotype_freq[indicem];
                  f5 = f5 + i5m * haplotype_freq[indicem];
                  f6 = f6 + i6m * haplotype_freq[indicem];
                }
              }
            }
          }
        }
      }
      final_gen=gen;
      break;
    }


  }



} //updated


int main(int argc, char *argv[])
{
    // check that the call of the program has the right number of parameters: 4, 5 or 6
    if (argc!=3)
    {
        cout << "need two mandatory parameters\n the parameter file name\t the output file \n the parameter file contains the different parameters in this order s1, s2, s3, s4, s5, s6, e12, e34, e56, r12, r23, r34, r45, r56 ";
        return -1;
    }

    // declare variables:
    // selection: s1,s2,s3,s4 ; epistasis e12, e13, e14 e23 e24 e34; dominance of the epistasis h12, h13, h14, h23, h24, h34; recombination r12, r23, r34
    // initial frequency f1,...f16.


    double s1, s2, s3, s4, s5, s6, e12, e34, e56,e13,e14,e15,e16,e23,e24,e25,e26,e35,e36,e45,e46, r12, r23, r34, r45, r56, mig_rate, f1=0, f2=0, f3=0, f4=0, f5=0, f6=0, change=0;
    int indicem, n_gen, mig_hap, ini_hap, final_gen, rounding_nb, cutoff_nb, fix_freq_nb;

    array<double, 64> fitness_table; // create fitness array
    array<double, 64> initial_freq; // initial frequencies array

    // read the aprameter file
    read_parameter(argv[1],s1, s2, s3, s4, s5, s6, e12, e34, e56, e13,e14,e15,e16,e23,e24,e25,e26,e35,e36,e45,e46,r12, r23, r34, r45, r56, n_gen, ini_hap, mig_hap, mig_rate, rounding_nb, cutoff_nb, fix_freq_nb);



    // open output file
    FILE * fichierO;
    fichierO= fopen(argv[2],"a");
    if (fichierO == NULL) {
            cout << "wrong output file" << endl;
        return -1;
    }

    // print header of the output file. It contains all the parameters value of the run, to ensure reapatability.
    fprintf(fichierO, "Parameters: s1=%f, s2=%f, s3=%f, s4=%f, s5=%f, s6=%f, e12=%f, e34=%f, e56=%f, e13=%f, e14=%f, e15=%f, e16=%f, e23=%f, e24=%f, e25=%f, e26=%f, e35=%f, e36=%f, e45=%f, e46=%f, r12=%f, r23=%f, r34=%f, r45=%f, r56=%f, n_gen=%i, mig_hap=%i, mig_rate=%f, ini_hap=%i \n", s1, s2, s3, s4, s5, s6, e12, e34, e56,e13,e14,e15,e16,e23,e24,e25,e26,e35,e36,e45,e46, r12,r23,r34,r45,r56, n_gen, mig_hap, mig_rate, ini_hap);



	// write initial freq and the migration table
	for (int i=0; i<64; i++){
		initial_freq[i]=0;
	}
	initial_freq[ini_hap]=1;


    // write the fitness table
    for (int i1m=0; i1m<=1; i1m++){
      for (int i2m=0; i2m<=1; i2m++){
        for (int i3m=0; i3m<=1; i3m++ ){
          for (int i4m=0; i4m<=1; i4m++){
            for (int i5m=0; i5m<=1; i5m++){
              for (int i6m=0; i6m<=1; i6m++){
                indicem =32*i1m+16*i2m+8*i3m+4*i4m+2*i5m+i6m;
                fitness_table[indicem]=s1*i1m+s2*i2m+s3*i3m+s4*i4m+s5*i5m+s6*i6m+i1m*i2m*e12+i3m*i4m*e34+i5m*i6m*e56+e13*i1m*i3m+e14*i1m*i4m+e15*i1m*i5m+e16*i1m*i6m+e23*i2m*i3m+e24*i2m*i4m+e25*i2m*i5m+e26*i2m*i6m+e35*i3m*i5m+e36*i3m*i6m+e45*i4m*i5m+e46*i4m*i6m;
              }
            }
          }
        }
      }
    }

  //  for (int i=0; i<64; i++){
   	//	cout << "hap"<<i<<"  "<<initial_freq[i]<< " ";
   		//cout << "h";
   // }

	final_gen=n_gen;
    // call main function
	run(n_gen, r12, r23, r34, r45, r56, mig_rate, mig_hap, fitness_table, initial_freq, f1, f2, f3, f4, f5, f6, final_gen, change, rounding_nb, cutoff_nb);

	fprintf(fichierO, "gen=%i, sum_of_changes=%e, Final freq. f1=%e, f2=%e, f3=%e, f4=%e, f5=%e, f6=%e \n", final_gen, change, f1, f2, f3, f4, f5, f6);

  fclose(fichierO);

  if (min(f1,min(f2,min(f3,min(f4,min(f5,f6)))))>pow(10,-fix_freq_nb) & (1-max(f1,max(f2,max(f3,max(f4,max(f5,f6))))))>pow(10,-fix_freq_nb)&final_gen<n_gen){
    cout << "TRUE" <<endl;
  } else {
    cout << "FALSE"  << endl;
  }


    return 1;
}
