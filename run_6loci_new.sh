#!/bin/bash

# first case no epistasis
# constraint: s1 is always the smallest and s2 is defined by s2=0.01 - sum(si) with i!=2

s1=0.01
s2=-0.01
s3=-0.01
s4=0.01
s5=0.01
s6=-0.01
e12=0
e34=0
e56=0
e13=0
e14=0
e15=0
e16=-0.12
e23=0
e24=-0.12
e25=0
e26=0
e35=-0.12
e36=0
e45=0
e46=0
r12=0.25
r23=0.25
r34=0.25
r45=0.25
r56=0.25
ngen=1000000
mig_hap=25 
ini_hap=38
round_prec=16
change_prec=10
freq_prec=6

declare -a epi=("0" "-0.003" "-0.006" "-0.0066" "-0.0075" "-0.009" "-0.12" "-0.24" "-0.36" "-0.48")
#declare -a epi=("0")

#declare -a arr=("0.00000100")
declare -a arr=("0.00000100" "0.00000114" "0.00000130" "0.00000149" "0.00000170" "0.00000194" "0.00000222" "0.00000253" "0.00000289" "0.00000330" "0.00000376" "0.00000430" "0.00000491" "0.00000560" "0.00000640" "0.00000730" "0.00000834" "0.00000952" "0.00001087" "0.00001241" "0.00001417" "0.00001618" "0.00001847" "0.00002109" "0.00002408" "0.00002749" "0.00003138" "0.00003583" "0.00004091" "0.00004671" "0.00005333" "0.00006089" "0.00006952" "0.00007937" "0.00009062" "0.00010346" "0.00011813" "0.00013487" "0.00015399" "0.00017581" "0.00020073" "0.00022918" "0.00026166" "0.00029875" "0.00034110" "0.00038944" "0.00044464" "0.00050766" "0.00057961" "0.00066176" "0.00075556" "0.00086265" "0.00098491" "0.00112451" "0.00128389" "0.00146587" "0.00167363" "0.00191084" "0.00218168" "0.00249090" "0.00284395" "0.00324703" "0.00370725" "0.00423270" "0.00483262" "0.00551757" "0.00629961" "0.00719248" "0.00821191" "0.00937582" "0.01070471" "0.01222194" "0.01395422" "0.01593202" "0.01819015" "0.02076833" "0.02371193" "0.02707274" "0.03090990" "0.03529092" "0.04029288" "0.04600379" "0.05252415" "0.05996866" "0.06846832" "0.07817269" "0.08925250" "0.10190272" "0.11634591" "0.13283621" "0.15166376" "0.17315984" "0.19770267" "0.22572408" "0.25771710" "0.29424466" "0.33594946" "0.38356529" "0.43792995" "0.50000000")


for ep in "${epi[@]}"
do
	for k in "${arr[@]}"
	do
		n1=0
		n2=0
		n3=0
		n4=0
		i=1
		while (( i < 10))
		do


			echo "0.0$i"
			echo "$s1 $s2 $s3 $s4 $s5 $s6 $e12 $e34 $e56 $e13 $e14 $e15 $ep $e23 $ep $e25 $e26 $ep $e36 $e45 $e46 $k $k $k $k $k $ngen $nind $mig_hap 0.0$i $ini_hap $round_prec $change_prec $freq_prec" > parameter.txt

			./6loci.exe parameter.txt outcome_6loc_new.txt >> log.txt
		


			var=$(awk 'END {print $1}' log.txt)

			if $var  
				then
					n1=$i
					i=$(($i+1))
				else
					break
			fi


		done

		echo "n1=$n1"
		i=1
		while (( i < 10))
		do

			echo "0.0$n1$i"	
			echo "$s1 $s2 $s3 $s4 $s5 $s6 $e12 $e34 $e56 $e13 $e14 $e15 $ep $e23 $ep $e25 $e26 $ep $e36 $e45 $e46 $k $k $k $k $k $ngen $nind $mig_hap 0.0$n1$i $ini_hap $round_prec $change_prec $freq_prec" > parameter.txt

			./6loci.exe parameter.txt outcome_6loc_new.txt >> log.txt


			var=$(awk 'END {print $1}' log.txt)

			if $var 
				then
					n2=$i
					i=$(($i+1))
				else
					break
			fi


		done

		echo "n2=$n2"
		i=1
		while (( i < 10))
		do

			echo "0.0$n1$n2$i"
			echo "$s1 $s2 $s3 $s4 $s5 $s6 $e12 $e34 $e56 $e13 $e14 $e15 $ep $e23 $ep $e25 $e26 $ep $e36 $e45 $e46 $k $k $k $k $k $ngen $nind $mig_hap 0.0$n1$n2$i $ini_hap $round_prec $change_prec $freq_prec" > parameter.txt

			./6loci.exe parameter.txt outcome_6loc_new.txt >> log.txt

		
			var=$(awk 'END {print $1}' log.txt)

			if $var 
				then
					n3=$i
					i=$(($i+1))
				else
					break
			fi


		done

		echo "n3=$n3"

		i=1
		while (( i < 10))
		do

			echo "0.0$n1$n2$n3$i"
			echo "$s1 $s2 $s3 $s4 $s5 $s6 $e12 $e34 $e56 $e13 $e14 $e15 $ep $e23 $ep $e25 $e26 $ep $e36 $e45 $e46 $k $k $k $k $k $ngen $nind $mig_hap 0.0$n1$n2$n3$i $ini_hap $round_prec $change_prec $freq_prec" > parameter.txt

			./6loci.exe parameter.txt outcome_6loc_new.txt >> log.txt


			var=$(awk 'END {print $1}' log.txt)

			if $var 
				then
					n4=$i
					i=$(($i+1))
				else
					break
			fi


		done

		echo "n4=$n4"

		echo echo "$s1 $s2 $s3 $s4 $s5 $s6 $e12 $e34 $e56 $e13 $e14 $e15 $ep $e23 $ep $e25 $e26 $ep $e36 $e45 $e46 $k $k $k $k $k $ngen $nind $mig_hap 0.0$n1$n2$n3$n4 $ini_hap $round_prec $change_prec $freq_prec" >> final_data_new.txt
	done
done
